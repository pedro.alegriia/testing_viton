<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Models\User;
class DiffDateMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
         $fecha_actual = date("Y-m-d");
         $fecha1 = date_create($fecha_actual);
         $query = User::where('correo',$request->correo)->first();
         $fecha2 = date_create($query->updated_at);
         $dias = date_diff($fecha1, $fecha2)->format(' %a ');
         if ($dias >0) {
             return redirect('sessiones');
          }
        return $next($request);
    }
}
