<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Models\User;
use Illuminate\Support\Facades\DB;
class CookieMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
         $query = User::with('userRoles')
         ->where('correo',$request->correo)
         ->get();

         $users = DB::table('users')
            ->leftJoin('user_has_roles', 'users.id', '=', 'user_has_roles.user_id')
             ->rightJoin('roles', 'user_has_roles.role_id', '=', 'roles.id')
               ->where('correo',$request->correo)
               ->where('user_has_roles.role_id',1)
                  ->select('users.id','users.nombre','users.apellido','users.correo',
                   'users.telefono','user_has_roles.*','roles.id as role_id' ,'roles.nombre as role')
            ->get();

          $ip =   $_SERVER['REMOTE_ADDR'];
        $verifica_role = $users[0]->role_id;
      
        if ($verifica_role ==1 && $ip == '127.0.0.1') {
            return response('cookie')->cookie('origin_sesion ', 'Test de cookie');  
        }

    }
}
