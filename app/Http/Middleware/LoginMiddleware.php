<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Models\User;
class LoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle($request, Closure $next)
    {

 
        $query = User::where('correo',$request->correo)->first();
        if ($query['correo_verified_at'] == "") {
             return redirect('verificacion');
        }else{
            return $next($request);
        }
 
    }
}
