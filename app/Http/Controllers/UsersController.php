<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Roles;
use Carbon\Carbon;
use App\Models\Permissions;
use Illuminate\Support\Facades\Auth;
use App\Models\UserRoles;
use App\Mail\Mail;
use App\Models\RolePermissions;
use \stdClass;
use Illuminate\Support\Facades\Validator;
class UsersController extends Controller
{
    public function index()
    {
        try {
             $data = User::with('roles','roles.role')->OrderBy('nombre','ASC')->get();

            $map = $data->map(function($info) {
                $map_permissions=  $info->roles->map(function($rol){
                   $permissions = RolePermissions::with('permisos')->where('role_id',$rol->role_id)->get();
                   return $permissions;
                  }); 
                   
                    $dataObj = new \stdClass();
                    $dataObj->id = $info->id; 
                    $dataObj->nombre = $info->nombre; 
                    $dataObj->apellido = $info->apellido; 
                    $dataObj->roles = $info->roles; 
                    $dataObj->permisos = $map_permissions; 
                      return $dataObj;
                    }); 
            return response()->json(['data' => $map, 'statusCode' => 200],200);
        } catch (Exception $e) {
               return response()->json(['errors' => $e->getMessage()],403);
        }
    }



      public function login(Request $request)
    {

      $input = $request->all();

       $this->validate($request, [
           'correo' => 'required',
           'password' => 'required',
       ]);

       $fieldType = filter_var($request->username, FILTER_VALIDATE_EMAIL) ? 'correo' : 'correo';
       if(auth()->attempt(array($fieldType => $input['correo'], 'password' => $input['password'])))
       {
         $user = $request->user();
       
          $user = $request->user();
          $mytime = Carbon::now();
          $time_now=  $mytime->toDateTimeString();
          $search = User::find($user->id);
          $search->updated_at = $time_now;
          $search->update();
        //  $request->session()->regenerate();
            return response()->json(['data' => $user, 'statusCode' => 200],200);
       }else{
          return response()->json(['error' => "datos incorrectos", 'statusCode' => 400],400);
       }

    }
    
     public function login_action(Request $request)
    {
        $request->validate([
            'correo' => 'required',
            'password' => 'required',
        ]);
        if (Auth::attempt(['correo' => $request->correo, 'password' => $request->password])) {
           // $request->session()->regenerate();
            return redirect('sessiones');
        }

        return back()->withErrors([
            'password' => 'Wrong username or password',
        ]);
    }
     

    public function showUserByRoleOneAndtwo()
    {
      $num = [1,2];
      try {
         $search =UserRoles::whereIn('role_id',$num)->get();
         $map = $search->map(function($val) {
                 $query =User::where('id',$val['user_id'])->get();
                return $query;
              }); 
              return response()->json(['data' => $map, 'statusCode' => 200],200);
       
        
      } catch (Exception $e) {
          return response()->json(['errors' => $e->getMessage()],403);
      }
    }

    public function showUserByRoleByPermissionTwo()
    {
       $num = 2;
      try {
            $search =Permissions::where('id',$num)->first();
            $search_rol_has_permission =RolePermissions::with('role')->where('permission_id',$search['id'])->get();
           
          
            $test = $search_rol_has_permission->map(function($val) {
              $filter_role = $val['role'];
               $rol = $filter_role->map(function($info) {

            $search_rol_has_user=UserRoles::query()
                ->with(['users' => function ($query) {
                    $query->select('id', 'nombre','apellido','correo','telefono','correo_verified_at','code');
                }])
                ->select('role_id','user_id')
                ->where('role_id',$info['id'])
                ->get();
                 return $search_rol_has_user;
                 }); 

            $dataObj = new \stdClass(); 
            $dataObj->users = $rol; 
            return $dataObj;
            });
            return $test[0]; 
      } catch (Exception $e) {
          return response()->json(['errors' => $e->getMessage()],403);
      }

    }

    public function sessiones()
    {
      return view('sessiones');
    }
     public function verificacion()
    {
      return view('verificacion');
    }

    public function sendEmail($correo,$code){
       Mail::to($correo)->send(new Mail($code));
      
    }
    public function createToken(){
       $token = $request->user()->createToken($request->token_name);
 
     return ['token' => $token->plainTextToken];
    }
}
