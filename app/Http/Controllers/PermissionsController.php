<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RolePermissions;
use App\Models\Permissions;
class PermissionsController extends Controller
{
    public function showPermissionByRole()
    {
        $num = 1;
        try {
             $search =RolePermissions::where('role_id',$num)->get();

            $map = $search->map(function($val) {
                 $query =Permissions::where('id',$val['permission_id'])->get();
                return $query;
              }); 
               return response()->json(['data' => $map, 'statusCode' => 200],200);
       
        } catch (Exception $e) {
          return response()->json(['errors' => $e->getMessage()],403);
       
      }
        
    }
}
