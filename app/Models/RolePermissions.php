<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RolePermissions extends Model
{
    protected $table = 'role_has_permissions';
    use HasFactory;

     protected $fillable = [
        'role_id',
        'permission_id',
    ];

        public function permisos()
    {
        return $this->hasMany('App\Models\Permissions', 'id');
    }

         public function role()
    {
        return $this->hasMany('App\Models\Roles','id','role_id');
    }
}
