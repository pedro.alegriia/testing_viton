<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    use HasFactory;

      protected $fillable = [
        'nombre',
    ];

      public function roles_has_permission()
    {
        return $this->hasMany('App\Models\RolePermissions', 'role_id');
    }

}
