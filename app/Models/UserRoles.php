<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserRoles extends Model
{
    use HasFactory;

     protected $table = 'user_has_roles';
     protected $fillable = [
        'role_id',
        'user_id',
    ];

      public function edificios()
    {
        return $this->hasOne(Edificios::class, 'plantel_id');
    }

      public function users()
    {
        return $this->hasMany('App\Models\User', 'id', 'user_id');
    }

    public function role()
    {
        return $this->hasMany('App\Models\Roles', 'id');
    }
}
