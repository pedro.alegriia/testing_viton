<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\PermissionsController;
 
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
 Route::post('login', [UsersController::class, 'login_action']);
   
 //   Route::get('users', 'UsersController@index');
   

Route::middleware('auth:sanctum')->group(function() {
        Route::get('users', [UsersController::class, 'index']);
        Route::get('user/{id}', 'UsersController@show');
        Route::get('showUserByRoleOneAndtwo', [UsersController::class, 'showUserByRoleOneAndtwo']);
        Route::get('showUserByRoleByPermissionTwo', [UsersController::class, 'showUserByRoleByPermissionTwo']);
    
        Route::get('sessiones', [UsersController::class, 'sessiones']);
        Route::get('roles', 'RolesController@index');

        Route::get('permissions', 'PermissionsController@index');
        Route::get('showPermissionByRole', [PermissionsController::class, 'showPermissionByRole']);
    
        Route::get('permissions', 'PermissionsController@index');
});

Route::get('verificacion', [UsersController::class, 'verificacion']);
       