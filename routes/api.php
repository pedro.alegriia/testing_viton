<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsersController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});




Route::middleware('login')->group(function () {
    Route::post('login', [UsersController::class, 'login']);
});

Route::middleware('diffDate')->group(function () {
    Route::post('loginDiff', [UsersController::class, 'login']);
});

Route::middleware('cookie')->group(function () {
    Route::post('loginCookie', [UsersController::class, 'login']);
});
