<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use   \App\Models\Roles;
use   \App\Models\User;
/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class UserRoleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
         return [
        'role_id' => Roles::factory(),
        'user_id' => User::factory(),
];
    }
}
