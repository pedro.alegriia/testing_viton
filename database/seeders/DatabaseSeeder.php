<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use \App\Models\User;
use   \App\Models\Permissions;
use   \App\Models\UserRoles;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
         \App\Models\User::factory(10)->create();
          \App\Models\Permissions::factory(10)->create();
           \App\Models\Roles::factory(10)->create();
        //\App\Models\UserRoles::factory(10)->create();

    }
}
